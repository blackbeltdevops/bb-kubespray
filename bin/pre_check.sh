#!/bin/bash
set -e
if [ -n "${DEBUG}" ];then set -x;fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
_BUILD_ID=$(date +"%m%d-%H%M%S")
CONFIG_INI="${DIR}/../host/kubespray.config.ini"

if [ ! -f ${CONFIG_INI} ];then
  cat <<EOF > ${PLYBK}/"${CONFIG_INI}"
master:
    user_name:         '' # MACHINE USER NAME
    host_address:      0  # IP ADDRESS
    ssh_password:      '' # SSH PASSWORD IF THERE ANY
    ssh_pk_file_name: '' # SSH PRIVATE KEY FILE NAME
    sudo_password:     '' # SUDO PASSWORD
worker:
    user_name:         '' # MACHINE USER NAME
    host_address:      0  # IP ADDRESS
    ssh_password:      '' # SSH PASSWORD IF THERE ANY
    ssh_pk_file_name: '' # SSH PRIVATE KEY FILE NAME
    sudo_password:     '' # SUDO PASSWORD
proxy:
    address: 0
    port: 0

## EXAMPLE ##
# master:
#     user_name:         'admin'               
#     host_address:      1.2.3.4               
#     ssh_password:      'top-secret-password' 
#     ssh_pk_file_name: 'my-private-key'
#     sudo_password:     'qwert1234' 
# 
# worker:
#     user_name:         'centos' 
#     host_address:      4.3.2.1
#     ssh_password:      'system'
#     ssh_pk_file_name: 'centos-private-key'
#     sudo_password:     'centos'
# 
# proxy:
#     address: 1.2.3.4
#     port:    3128
EOF
  echo "Please fill the required fields in ${CONFIG_INI} file and run again the script !! ( USER, IP mandatory fields ) "
  exit
else
  machines=( master worker )
  for i in "${machines[@]}"
  do
    declare "${i}_user_name"="$(cat ${CONFIG_INI} | shyaml get-value $i.user_name)"
    declare "${i}_host_address"="$(cat ${CONFIG_INI} | shyaml get-value $i.host_address)"
    declare "${i}_ssh_password"="$(cat ${CONFIG_INI} | shyaml get-value $i.ssh_password)"
    declare "${i}_ssh_pk_file_name"="$(cat ${CONFIG_INI} | shyaml get-value $i.ssh_pk_file_name)"
    declare "${i}_sudo_password"="$(cat ${CONFIG_INI} | shyaml get-value $i.sudo_password)"
  done
  proxy_address=$(cat ${CONFIG_INI} | shyaml get-value proxy.address)
  proxy_port=$(cat ${CONFIG_INI} | shyaml get-value proxy.port)

  for i in "${machines[@]}"
  do
    eval "echo \${$(echo ${i}_user_name):?Must be set} > /dev/null"
    eval "echo \${$(echo ${i}_host_address):?Must be set} > /dev/null"
  done
fi
##########################

PLYBK=playbooks

##########################
# Kubespray config files #
##########################
cat <<EOF >> ${PLYBK}/kubespray/inventory/bb-cluster/group_vars/k8s-cluster.yml
supplementary_addresses_in_ssl_keys: [ ${master_host_address} ]
EOF

if [ -n "${proxy_address}" ];then
  proxy_port=${proxy_port:-3128}
cat <<EOF >> ${PLYBK}/kubespray/inventory/bb-cluster/group_vars/all.yml
http_proxy:  "http://${proxy_address}:${proxy_port}"
https_proxy: "http://${proxy_address}:${proxy_port}"
EOF
fi

if [ -n "${master_ssh_pk_file_name}" ]; then master_pk_file="ansible_ssh_private_key_file=${DIR}/../host/${master_ssh_pk_file_name}"; fi
if [ -n "${worker_ssh_pk_file_name}" ]; then worker_pk_file="ansible_ssh_private_key_file=${DIR}/../host/${worker_ssh_pk_file_name}"; fi

cat <<EOF > ${PLYBK}/kubespray/inventory/bb-cluster/hosts.ini
[all]
master   ansible_user=${master_user_name} ansible_host=${master_host_address} ansible_ssh_pass=${master_ssh_password} ansible_sudo_pass=${master_sudo_password} ansible_become_pass=${master_sudo_password} ${master_pk_file}
worker-1 ansible_user=${worker_user_name} ansible_host=${worker_host_address} ansible_ssh_pass=${worker_ssh_password} ansible_sudo_pass=${worker_sudo_password} ansible_become_pass=${worker_sudo_password} ${worker_pk_file}

[kube-master]
master

[kube-node]
worker-1

[etcd]
master

[k8s-cluster:children]
kube-node
kube-master

[calico-rr]

[vault]
master
worker-1

EOF

#################################################
# Copy and edit admin.conf ( kube config file ) #
#################################################
cat <<EOF > ${PLYBK}/kube-conf/kube-conf-host.ini
[all]
master   ansible_user=${master_user_name} ansible_host=${master_host_address} ansible_ssh_pass=${master_ssh_password} ansible_sudo_pass=${master_sudo_password} ansible_become_pass=${master_sudo_password} ${master_pk_file}
EOF

cat <<EOF > ${PLYBK}/firewall/firewall.ini
[all]
master   ansible_user=${master_user_name} ansible_host=${master_host_address} ansible_ssh_pass=${master_ssh_password} ansible_sudo_pass=${master_sudo_password} ansible_become_pass=${master_sudo_password} ${master_pk_file}
worker-1 ansible_user=${worker_user_name} ansible_host=${worker_host_address} ansible_ssh_pass=${worker_ssh_password} ansible_sudo_pass=${worker_sudo_password} ansible_become_pass=${worker_sudo_password} ${worker_pk_file}
EOF

########################
# CI Test config files #
########################
if [ -n "$CI_TEST" ]; then
  cat <<EOF > ${PLYBK}/proxy/proxy.ini
[all]
proxy   ansible_user=${master_user_name} ansible_host=${proxy_address} ansible_ssh_pass=${master_ssh_password} ansible_sudo_pass=${master_sudo_password} ansible_become_pass=${master_sudo_password} ${master_pk_file}
EOF

  cat <<EOF > tests/kube-hello-world-test-host.ini
[all]
master   ansible_user=${master_user_name} ansible_host=${master_host_address} ansible_ssh_pass=${master_ssh_password} ansible_sudo_pass=${master_sudo_password} ansible_become_pass=${master_sudo_password} ${master_pk_file}
[all:vars]
proxy_server=${proxy_address}
proxy_port=${proxy_port}
EOF

fi

