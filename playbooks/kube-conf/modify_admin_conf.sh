#!/bin/bash
set -ex
export HOST_IP=$1
cat /etc/kubernetes/admin.conf | sed -r 's/server: https:\/\/([0-9]+\.?)+:6443/server: https:\/\/'${HOST_IP}':6443/g' > /tmp/admin.conf
cp /etc/kubernetes/admin.conf ~/
chown centos-k8s:centos-k8s ~/admin.conf
chmod +wr ~/admin.conf

