From ubuntu:16.04

RUN apt-get update && apt-get install -y locales python3 python-pip && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

WORKDIR /opt/ansible-installer

COPY playbooks/kubespray playbooks/kubespray 
RUN pip install -r playbooks/kubespray/requirements.txt shyaml

COPY bin bin
# COPY kube-conf kube-conf
# COPY firewalld firewalld
COPY playbooks playbooks

ENTRYPOINT ["./bin/entrypoint.sh"]

