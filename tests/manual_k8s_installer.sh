#!/bin/bash
set -ex

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

_BUILD_ID=${1:-$(date +"%m%d-%H%M%S")}
_VM_HOME_USER=centos-k8s
_IMAGE_FAMILY=centos-7
_IMAGE_PROJECT=centos-cloud
_CREATE_USER_WITH_ROOT='set -ex 
sudo $(sudo which useradd) -m ###USER### -s /bin/bash ;
sudo echo "###USER###" | sudo $(sudo which passwd) ###USER### --stdin ;
sudo $(sudo which usermod) -aG wheel ###USER### ;
sudo sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config ;
sudo $(sudo which service) sshd restart ;
sudo firewall-cmd --set-default-zone public ;
'
_PROXY_IMAGE_FAMILY='ubuntu-1604-lts'
_PROXY_IMAGE_PROJECT='ubuntu-os-cloud'
_PROXY_USER_WITH_ROOT='set -ex 
sudo $(sudo which useradd) -m ###USER### -s /bin/bash  ;
echo ###USER###:###USER### | sudo $(sudo which chpasswd)  ;
sudo $(sudo which usermod) -aG sudo ###USER###  ;
sudo sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config  ;
sudo $(sudo which service) sshd restart ;
'
# _PROJECT_NAME=nexus-clean-proto
_PROJECT_NAME=anakin2-216418

if [ -z "$1"  ];then
  
  gcloud config set project ${_PROJECT_NAME}
  
  gcloud compute instances create kube-spray-deployer-${_BUILD_ID} \
              --metadata enable-oslogin=TRUE \
              --zone europe-west1-b \
              --machine-type f1-micro \
              --image-family ${_PROXY_IMAGE_FAMILY} \
              --image-project ${_PROXY_IMAGE_PROJECT} \
              --labels machine=deployer-${_BUILD_ID} \
              --boot-disk-type pd-ssd  \
              --boot-disk-size 50 \
              --boot-disk-device-name kube-s-deployer-${_BUILD_ID} \
              --tags kube-spray-k8s-${_BUILD_ID} --async
  
  gcloud compute instances create kube-spray-proxy-${_BUILD_ID} \
              --metadata enable-oslogin=TRUE \
              --zone europe-west1-b \
              --machine-type f1-micro \
              --image-family ${_PROXY_IMAGE_FAMILY} \
              --image-project ${_PROXY_IMAGE_PROJECT} \
              --labels machine=proxy-${_BUILD_ID} \
              --boot-disk-type pd-ssd  \
              --boot-disk-size 50 \
              --boot-disk-device-name kube-s-proxy-${_BUILD_ID} \
              --tags kube-spray-proxy-${_BUILD_ID} --async
  
  gcloud compute instances create kube-spray-master-${_BUILD_ID} \
              --metadata enable-oslogin=TRUE \
              --zone europe-west1-b \
              --machine-type n1-standard-2 \
              --image-family ${_IMAGE_FAMILY} \
              --image-project ${_IMAGE_PROJECT} \
              --labels machine=master-${_BUILD_ID} \
              --boot-disk-type pd-ssd  \
              --boot-disk-size 50 \
              --boot-disk-device-name kube-s-master-${_BUILD_ID} \
              --tags kube-spray-k8s-${_BUILD_ID} --async
  
  gcloud compute instances create kube-spray-worker-${_BUILD_ID} \
              --metadata enable-oslogin=TRUE \
              --zone europe-west1-b \
              --machine-type n1-standard-4 \
              --image-family ${_IMAGE_FAMILY} \
              --image-project ${_IMAGE_PROJECT} \
              --labels machine=worker-${_BUILD_ID} \
              --boot-disk-type pd-ssd  \
              --boot-disk-size 50 \
              --boot-disk-device-name kube-s-worker-${_BUILD_ID} \
              --tags kube-spray-k8s-${_BUILD_ID}
  
  sleep 10
  user_creator=$(echo "${_CREATE_USER_WITH_ROOT}" | sed -r "s/###USER###/${_VM_HOME_USER}/g")
  proxy_user_creator=$(echo "${_PROXY_USER_WITH_ROOT}" | sed -r "s/###USER###/${_VM_HOME_USER}/g")
  
  array=( proxy master worker deployer )
  
  for i in "${array[@]}"
  do
    if [ "$i" == "proxy" -o "$i" == "deployer" ]; then
      gcloud compute ssh kube-spray-$i-${_BUILD_ID} --zone europe-west1-b --quiet --ssh-flag="-T" --command "$proxy_user_creator"
    else
      gcloud compute ssh kube-spray-$i-${_BUILD_ID} --zone europe-west1-b --quiet --ssh-flag="-T" --command "$user_creator"
    fi
  done
  
  work_dir=${DIR}/../\k8s.${_BUILD_ID}
  mkdir $work_dir && cd $work_dir
  
  echo ${_BUILD_ID} > _BUILD_ID
  
  # cp ~/.ssh/google_compute_engine ssh-private-key
  # echo "${_VM_HOME_USER}":$(cat ~/.ssh/google_compute_engine.pub) > ssh-private-key.pub
  
  for i in "${array[@]}"
  do
    gcloud compute instances remove-metadata kube-spray-$i-${_BUILD_ID} --zone europe-west1-b --keys=enable-oslogin
    # gcloud compute instances add-metadata kube-spray-$i-${_BUILD_ID} --zone europe-west1-b --metadata-from-file ssh-keys=ssh-private-key.pub
    gcloud compute instances list --filter="labels.machine:$i-${_BUILD_ID}" --format="value(networkInterfaces[0].accessConfigs[0].natIP)" > "$i"_ip
  done
  
  # internal_ip=$(gcloud compute instances list  --filter="labels.machine:master-${_BUILD_ID}" --format="value(networkInterfaces[0].networkIP)" | grep -oP "\d+\.\d+\.")0.0/16
  
  gcloud compute firewall-rules create kube-spray-proxy-${_BUILD_ID} --allow tcp:3128 --target-tags kube-spray-proxy-${_BUILD_ID}
  gcloud compute firewall-rules create kube-spray-k8s-${_BUILD_ID}-enable-all-tcp-out --direction EGRESS  --allow tcp --target-tags kube-spray-k8s-${_BUILD_ID} --priority 10
  gcloud compute firewall-rules create kube-spray-k8s-${_BUILD_ID}-enable-all-tcp-in  --direction INGRESS --allow tcp --target-tags kube-spray-k8s-${_BUILD_ID} --priority 10
  
  # cp ssh-private-key ../
  sleep 10
fi

if [ ! -d "anin-config" ]; then mkdir "anin-config"; fi
    echo "
master:
    user_name:         ${_VM_HOME_USER}
    host_address:      $(cat master_ip)
    ssh_password:      ${_VM_HOME_USER}
#    ssh_pk_file_name:  ${_VM_HOME_USER}
    sudo_password:     ${_VM_HOME_USER}
worker:
    user_name:         ${_VM_HOME_USER}
    host_address:      $(cat worker_ip)
    ssh_password:      ${_VM_HOME_USER}
#    ssh_pk_file_name:  ${_VM_HOME_USER}
    sudo_password:     ${_VM_HOME_USER}
proxy:
    address: $(cat proxy_ip) 
    port: 3128
" > anin-config/kubespray.config.ini

PROJECT_NAME=${_PROJECT_NAME} DEBUG=ON CI_TEST=ON ${DIR}/../run.sh

