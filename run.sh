#!/bin/bash
set -e
if [ -n "${DEBUG}" ];then set -x;fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
_BUILD_ID=$(date +"%m%d-%H%M%S")
_PROJECT_NAME=${PROJECT_NAME:-}

if [ -z "$CI_TEST" ]; then
  DOCKER_FLAG="-it"
else
  ENV_N_MOUNT_FLAG="-e CI_TEST=${CI_TEST} -e DEBUG=${DEBUG} -v ${DIR}/tests:/opt/ansible-installer/tests"
fi

if [ ! -d $(pwd)/anin-config ];then mkdir $(pwd)/anin-config; fi
docker run ${DOCKER_FLAG} \
  ${ENV_N_MOUNT_FLAG} \
  --name anakin-k8s-${_BUILD_ID} -v $(pwd)/anin-config:/opt/ansible-installer/host --rm gcr.io/${PROJECT_NAME}/anakin-k8s:latest

