#!/bin/bash
# entrypoint.sh
# at: bin/
set -e

if [ -n "${DEBUG}" ];then set -x; fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

. ${DIR}/pre_check.sh
PLYBK=playbooks

if [ -n "${CI_TEST}" ];then cd ${PLYBK}/proxy && cat proxy.ini && ansible-playbook -b -i proxy.ini squid.yml && cd ${DIR}/..; fi 

if [ -n "${DEBUG}" ];then 
   cat ${PLYBK}/kubespray/inventory/bb-cluster/group_vars/k8s-cluster.yml | tail
   cat ${PLYBK}/kubespray/inventory/bb-cluster/group_vars/all.yml | tail
   cat ${PLYBK}/kubespray/inventory/bb-cluster/hosts.ini
   cat ${PLYBK}/firewall/firewall.ini
fi

cd ${PLYBK}/firewall && ansible-playbook -b -i firewall.ini firewall.yml && cd ${DIR}/..
cd ${PLYBK}/kubespray && ansible-playbook -b -i inventory/bb-cluster/hosts.ini cluster.yml && cd ${DIR}/..
cd ${PLYBK}/kube-conf && ansible-playbook -b -i kube-conf-host.ini save_kube_config.yml && cd ${DIR}/..

# Test k8s with hello world nginx server...
if [ -n "${CI_TEST}" ];then cd tests && ansible-playbook -i kube-hello-world-test-host.ini shell.yml; fi

